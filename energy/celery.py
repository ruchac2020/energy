from celery import Celery
from celery.schedules import crontab
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'energy.settings')
os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')
app = Celery('energy')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


app.conf.beat_schedule = {
    # Executes every Monday morning at 7:30 a.m.
    'generate_csv': {
        'task': 'generate_csv',
        'schedule': crontab(minute=27, hour=15)  #crontab(minute='*/1')
        },
    "write_to_db": {
        'task': 'write_to_db',
        'schedule': crontab(minute=45, hour=15)
       }
}
